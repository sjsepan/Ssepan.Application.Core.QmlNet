# readme.md - README for Ssepan.Application.Core.QmlNet v5.2

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.
Implement code specific to Qml.Net.

### Usage notes

~...

### History

6.0:

~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.2:
~Clone from Ssepan.Application.Core.* libs

### Issues

-Cannot access static .Net Dialogs class or members, and cannot store instance of Dialogs in qml between signals (OnOpenXxxDialog, OnAccepted/OnRejected). Using Singleton pattern is suggested for C++ users of Qml, but implementing in C# is less clear. Ended up using a DialogsController class wrapped around the Dialogs class, and a GetNetObject method to hide static nature from Qml.
<https://github.com/qmlnet/qmlnet/issues/78>

### Contact

Steve Sepan
<sjsepan@yahoo.com>
3/6/2024
