﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Qml.Net;

namespace Ssepan.Application.Core.QmlNet
{
	/// <summary>
	/// Note: this class can be subclassed without type parameters in the client.
	/// </summary>
	/// <typeparam name="TIcon">TIcon</typeparam>
	/// <typeparam name="TSettings">TSettings</typeparam>
	/// <typeparam name="TModel">TModel</typeparam>
	/// <typeparam name="TView">TView</typeparam>
	[Signal("propertyChanged", NetVariantType.String)]
    public class FormsViewModel
    <
        TIcon,
        TSettings,
        TModel,
        TView
    > :
        ViewModelBase<TIcon>
        where TIcon : class
        where TSettings : class, ISettings, new()
        where TModel : class, IModel, new()
        //Constraint cannot be special class 'object' [Ssepan.Application.Core.QmlNet]csharp(CS0702)
        //where TView : object//Note: not sure how to access many Qt UI types w/ Qml.Net Window; can't use object either
    {
        #region Declarations
        public const string Button_OK = "Ok";
        public const string Button_Cancel = "Cancel";
        public const string Button_Yes = "Yes";
        public const string Button_No = "No";
        public const string Button_None = "None";

        //public delegate bool DoWork_WorkDelegate(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);
        public delegate TReturn DoWork_WorkDelegate<TReturn>(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);

        protected static FileDialogInfo<object, string> _settingsFileDialogInfo;
        protected Dictionary<string, TIcon> _actionIconImages;

        #endregion Declarations

        #region Constructors
        public FormsViewModel()
        {
            if (SettingsController<TSettings>.Settings == null)
            {
                SettingsController<TSettings>.New();
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo
        ) :
            this()
        {
            try
            {
                //(and the delegate it contains
                if (propertyChangedEventHandlerDelegate != default)
                {
                    PropertyChanged += new PropertyChangedEventHandler(propertyChangedEventHandlerDelegate);
                }

                _actionIconImages = actionIconImages;

                _settingsFileDialogInfo = settingsFileDialogInfo;

                ActionIconImage = _actionIconImages["Save"];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo,
            TView view //= default(TView) //(In VB 2010, )VB caller cannot differentiate between members which differ only by an optional param--SJS
        ) :
            this(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
                View = view;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

		#endregion Constructors
		#region Properties
		public TView View { get; set; }
		#endregion Properties

		#region Methods
		#region Menus
		#region menu feature boilerplate
		//may be used as-is, or overridden to handle differently

		public async virtual Task FileNew()
        {
            MessageDialogInfo<object, string, object, string, string> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "New" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["New"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    questionMessageDialogInfo =
                        new MessageDialogInfo<object, string, object, string, string>
                        (
                            parent: View,//saved Window reference
                            modal: true,
                            title: "New",
                            dialogFlags: null,
                            messageType: "Question",
                            buttonsType: "YesNo",
                            message: "Save changes?",
                            response: Button_None
                        );

                    questionMessageDialogInfo = await DialogsController.Dialogs.Instance.ShowMessageDialog(questionMessageDialogInfo);

                    if (!questionMessageDialogInfo.BoolResult)
                    {
                        throw new ApplicationException(string.Format("ShowMessageDialog: {0}", questionMessageDialogInfo.ErrorMessage));
                    }

                    if (questionMessageDialogInfo.Response == Button_Yes)
                    {
                        //SAVE
                        await FileSaveAs();
                    }
                    // else treat as cancel
                }

                //NEW
                if (!SettingsController<TSettings>.New())
                {
                    throw new ApplicationException(string.Format("Unable to get New settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar("", string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Open object at SettingsController<TSettings>.FilePath.
        /// </summary>
        /// <param name="forceDialog">If false, just use SettingsController<TSettings>.FilePath</param>
        public async virtual Task FileOpen(bool forceDialog = true)
        {
            MessageDialogInfo<object, string, object, string, string> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Open" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Open"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    questionMessageDialogInfo =
                        new MessageDialogInfo<object, string, object, string, string>
                        (
                            parent: View,//saved Window reference
                            modal: true,
                            title: "Open...",
                            dialogFlags: null,
                            messageType: "Question",
                            buttonsType: "YesNo",
                            message: "Save changes?",
                            response: Button_None
                        );

                    questionMessageDialogInfo = await DialogsController.Dialogs.Instance.ShowMessageDialog(questionMessageDialogInfo);

                    if (!questionMessageDialogInfo.BoolResult)
                    {
                        throw new ApplicationException(string.Format("ShowMessageDialog: {0}", questionMessageDialogInfo.ErrorMessage));
                    }

                    if (questionMessageDialogInfo.Response == Button_Yes)
                    {
                        //SAVE
                        await FileSave();
                    }
                    // else treat as cancel
                }

                if (forceDialog)
                {
                    _settingsFileDialogInfo.Title = "Open...";
                    // _settingsFileDialogInfo.ForceDialog = forceDialog;
                    // _settingsFileDialogInfo.Modal = true;
                    _settingsFileDialogInfo.MustExist = true;
                    _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;

                    _settingsFileDialogInfo = await DialogsController.Dialogs.Instance.GetPathForLoad(_settingsFileDialogInfo);

                    if (!_settingsFileDialogInfo.BoolResult)
                    {
                        throw new ApplicationException(string.Format("GetPathForLoad: {0}", _settingsFileDialogInfo.ErrorMessage));
                    }

                    if (_settingsFileDialogInfo.Response != Button_None)
                    {
                        if (_settingsFileDialogInfo.Response == Button_OK)
                        {
                            SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                            UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                        }
                        else
                        {
                            //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                        }
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }

                //OPEN
                if (!SettingsController<TSettings>.Open())
                {
                    throw new ApplicationException(string.Format("Unable to Open settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task FileSave(bool isSaveAs=false)
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Save" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Save"],
                    true,
                    33
                );

                _settingsFileDialogInfo.Title = isSaveAs ? "Save As..." : "Save...";
                _settingsFileDialogInfo.ForceDialog = isSaveAs;
                _settingsFileDialogInfo.Modal = true;
                _settingsFileDialogInfo.MustExist = false;
                _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;

                _settingsFileDialogInfo = await DialogsController.Dialogs.Instance.GetPathForSave(_settingsFileDialogInfo);

                if (!_settingsFileDialogInfo.BoolResult)
                {
                    throw new ApplicationException(string.Format("GetPathForSave: {0}", _settingsFileDialogInfo.ErrorMessage));
                }

                if (_settingsFileDialogInfo.Response != Button_None)
                {
                    if (_settingsFileDialogInfo.Response == Button_OK)
                    {
                        SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                        UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                //SAVE
                if (!SettingsController<TSettings>.Save())
                {
                    throw new ApplicationException(string.Format("Unable to Save settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task FileSaveAs()
        {
            await FileSave(true);
        }

        public async virtual Task FilePrint()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            PrinterDialogInfo<object, string, string> printerDialogInfo = null;

            try
            {
                StartProgressBar
                (
                    "Print" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Print"],
                    true,
                    33
                );

                //select printer
                printerDialogInfo = new PrinterDialogInfo<object, string, string>
                (
                    parent: View,
                    modal: true,
                    title: "Printer",
                    response: Button_None
                );

                await DialogsController.Dialogs.Instance.GetPrinter(printerDialogInfo);

                if (!printerDialogInfo.BoolResult)
                {
                    throw new ApplicationException(string.Format("GetPrinter: {0}", printerDialogInfo.ErrorMessage));
                }

                if (printerDialogInfo.Response != Button_None)
                {
                    if (printerDialogInfo.Response == Button_OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + printerDialogInfo.Printer + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                // }
                // else
                // {
                //     ErrorMessage = printerDialogInfo.ErrorMessage;
                // }

                //PRINT
                if (await Print())
                {
                    // StopProgressBar("Printed.");
                }
                else
                {
                    // StopProgressBar("Print cancelled.");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task FilePrintPreview()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Print Preview" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["PrintPreview"],
                    true,
                    33
                );

                //TODO:select printer?

                if (await PrintPreview())
                {
                    StopProgressBar(StatusMessage + ACTION_DONE);
                }
                else
                {
                    StopProgressBar(StatusMessage + ACTION_CANCELLED);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Returns true = Yes = Quit
        /// </summary>
        public async virtual Task<bool> FileExit()
        {
            bool resultOkQuitting = false;
            MessageDialogInfo<object, string, object, string, string> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Quit" + ACTION_IN_PROGRESS,
                    null,
                    null,
                    true,
                    33
                );

                // prompt before quitting
                questionMessageDialogInfo =
                    new MessageDialogInfo<object, string, object, string, string>
                    (
                        parent: View,//saved Window reference
                        modal: true,
                        title: "Quit?",
                        dialogFlags: null,
                        messageType: "Question",
                        buttonsType: "YesNo",
                        message: "Are you sure you want to quit?",
                        response: Button_None
                    );

                questionMessageDialogInfo = await DialogsController.Dialogs.Instance.ShowMessageDialog(questionMessageDialogInfo);

                if (!questionMessageDialogInfo.BoolResult)
                {
                    throw new ApplicationException("File Exit question dialog returned error");
                }

                if (questionMessageDialogInfo.Response == Button_No)
                {
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                }
                else
                {
                    resultOkQuitting = true;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("{0}", ex.Message);

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
             }
            return resultOkQuitting;
        }

        public async virtual Task EditUndo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Undo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Undo"],
                    true,
                    33
                );

                if (!await Undo())
                {
                    throw new ApplicationException("'Undo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditRedo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Redo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Redo"],
                    true,
                    33
                );

                if (!await Redo())
                {
                    throw new ApplicationException("'Redo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditSelectAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Select All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["SelectAll"],
                    true,
                    33
                );

                if (!await SelectAll())
                {
                    throw new ApplicationException("'Select All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditCut()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Cut" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Cut"],
                    true,
                    33
                );

                if (!await Cut())
                {
                    throw new ApplicationException("'Cut' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditCopy()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Copy" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Copy"],
                    true,
                    33
                );

                if (!await Copy())
                {
                    throw new ApplicationException("'Copy' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditPaste()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Paste"],
                    true,
                    33
                );

                if (!await Paste())
                {
                    throw new ApplicationException("'Paste' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditPasteSpecial()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past Special" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["PasteSpecial"],
                    true,
                    33
                );

                if (!await PasteSpecial())
                {
                    throw new ApplicationException("'Paste Special' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditDelete()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Delete" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Delete"],
                    true,
                    33
                );

                if (!await Delete())
                {
                    throw new ApplicationException("'Delete' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditFind()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Find" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Find"],
                    true,
                    33
                );

                if (!await Find())
                {
                    throw new ApplicationException("'Find' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditReplace()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Replace" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Replace"],
                    true,
                    33
                );

                if (!await Replace())
                {
                    throw new ApplicationException("'Replace' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditRefresh()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Refresh" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Refresh"],
                    true,
                    33
                );

                if (!await Refresh())
                {
                    throw new ApplicationException("'Refresh' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async Task EditPreferences()
        {
            FileDialogInfo<object, string> fileDialogInfo = null; //not strictly necessary since we're not passing it, but for example.
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            //for preferences, do a folder-path dialog demo 
            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

				fileDialogInfo =
					new FileDialogInfo<object, string>
					(
						parent: View,
						modal: true,
						title: "Open Folder...",
						response: Button_None
					)
					{
						Filename = null,
						MustExist = true,
						Multiselect = false,
						SelectFolders = true,
						Filters =
							string.Join
							(
								FileDialogInfo<object, string>.FILTER_SEPARATOR,
								[
									"All files (*.*)"
                                    // string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "All files", "*")//, 
                                ]
							)
					};

				fileDialogInfo = await DialogsController.Dialogs.Instance.GetFolderPath(fileDialogInfo);//folderDialog.ShowAsync(fileDialogInfo.Parent);

                if (!fileDialogInfo.BoolResult)
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", fileDialogInfo.ErrorMessage));
                }

                if (fileDialogInfo.Response != Button_None)
                {
                    if (fileDialogInfo.Response == Button_OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    // if (!string.IsNullOrWhiteSpace(fileDialogInfo.Filename))
                    // {
                    //     StopProgressBar(StatusMessage + fileDialogInfo.Filename + ACTION_DONE);
                    // }
                    // else
                    // {
                        //treat as cancel
                    //     StopProgressBar(StatusMessage + ACTION_CANCELLED);
                    // }
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task EditProperties()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Properties" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Properties"],
                    true,
                    33
                );

                if (!await Properties())
                {
                    throw new ApplicationException("'Properties' error");
                }

                //TODO:when implemented, a Properties dialog can also be cancelled, so handle cancel too
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowNewWindow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "New Window" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["NewWindow"],
                    true,
                    33
                );

                if (!await NewWindow())
                {
                    throw new ApplicationException("'New Window' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowTile()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Tile" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Tile"],
                    true,
                    33
                );

                if (!await Tile())
                {
                    throw new ApplicationException("'Tile' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowCascade()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Cascade" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Cascade"],
                    true,
                    33
                );

                if (!await Cascade())
                {
                    throw new ApplicationException("'Cascade' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowArrangeAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Arrange All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["ArrangeAll"],
                    true,
                    33
                );

                if (!await ArrangeAll())
                {
                    throw new ApplicationException("'Arrange All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowHide()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Hide" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Hide"],
                    true,
                    33
                );

                if (!await Hide())
                {
                    throw new ApplicationException("'Hide' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task WindowShow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Show" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Show"],
                    true,
                    33
                );

                if (!await Show())
                {
                    throw new ApplicationException("'Show' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task HelpContents()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Contents" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Contents"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!await Contents())
                {
                    throw new ApplicationException("'Help Contents' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task HelpIndex()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Index" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Index"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!await Index())
                {
                    throw new ApplicationException("'Help Index' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task HelpOnlineHelp()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Online Help" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["HelpOnlineHelp"],
                    true,
                    33
                );

                //System.Windows.Forms.Help
                if (!await OnlineHelp())
                {
                    throw new ApplicationException("'Online Help' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task HelpLicenceInformation()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Licence Information" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["LicenceInformation"],
                    true,
                    33
                );

                if (!await LicenceInformation())
                {
                    throw new ApplicationException("'Licence Information' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async Task HelpCheckForUpdates()
        {
            MessageDialogInfo<object, string, object, string, string> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Check For Updates" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["CheckForUpdates"],
                    true,
                    33
                );

                questionMessageDialogInfo =
                    new MessageDialogInfo<object, string, object, string, string>
                    (
                        View,//saved Window reference
                        true,
                        "Check for Updates",
                        null,
                        "Question",
                        "YesNo",
                        "Check for Updates?",
                        null
                    );

                questionMessageDialogInfo = await DialogsController.Dialogs.Instance.ShowMessageDialog(questionMessageDialogInfo);
                if (!questionMessageDialogInfo.BoolResult)
                {
                    throw new ApplicationException("Help Check for Updates dialog exited with error");
                }

                if (questionMessageDialogInfo.Response != null)
                {
                    if (questionMessageDialogInfo.Response == "Yes")
                    {
                        StopProgressBar(StatusMessage + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar(StatusMessage + ACTION_CANCELLED);
                    }
                }
                else
                {
                    throw new Exception("HelpCheckForUpdates:buttonResult:"+ questionMessageDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public async virtual Task HelpAbout<TAssemblyInfo>()
            where TAssemblyInfo :
            //class,
            AssemblyInfoBase<object>,
            new()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            TAssemblyInfo assemblyInfo = null;
            AboutDialogInfo<object, string, string> aboutDialogInfo = null;//TResponseEnum has to be string, because About dialog uses custom dialog

            try
            {
                StartProgressBar
                (
                    "About" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["About"],
                    true,
                    33
                );

                assemblyInfo = new TAssemblyInfo();
                // use GUI mode About feature
                aboutDialogInfo = new AboutDialogInfo<object, string, string>()
                {
                    Parent = View,
                    Response = default,
                    Modal = true,
                    Title = "About...",
                    ProgramName = assemblyInfo.Title,//"MvcForms.Core.QmlNet",
                    Version = assemblyInfo.Version,//"v0.7",
                    Copyright = assemblyInfo.Copyright,//"Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA",
                    Comments = assemblyInfo.Description,//"Desktop GUI app demo, on Linux, in C# / DotNet[5|6] / QmlNet, using VSCode / Glade.",
                    Website = assemblyInfo.Website,//"https://gitlab.com/sjsepan/MvcForms.Core.QmlNet",
                    Logo = "App"
                };

                //call async instance version
                aboutDialogInfo = await DialogsController.Dialogs.Instance.ShowAboutDialog(aboutDialogInfo);

                if (!aboutDialogInfo.BoolResult)
                {
                    throw new ApplicationException(aboutDialogInfo.ErrorMessage);
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        #endregion menu feature boilerplate

        #region menu feature implementation
        //likely to need overridden to handle feature specifics

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// with the print dialog, such as sending a document to the printer
        /// </summary>
        /// <returns>bool</returns>
        protected async virtual Task<bool> Print()
        {
            bool returnValue = default;

            try
            {
                    await DoSomething();

                    returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// </summary>
        /// <returns>bool</returns>
        protected async virtual Task<bool> PrintPreview()
        {
            bool returnValue = default;
            // string errorMessage = null;
            // PrinterDialogInfo printerDialoginfo = null;

            try
            {
                // if (Dialogs.Instance.GetPrinter(ref printerDialoginfo, ref errorMessage))
                // {
                //     Log.Write(printerDialoginfo.Printer.Name,  Log.EventLogEntryType_Error);
                    await DoSomething();

                     returnValue = true;
                // }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Undo()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Redo()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> SelectAll()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Cut()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Copy()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Paste()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> PasteSpecial()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Delete()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Find()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Replace()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Refresh()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Preferences()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Properties()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> NewWindow()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Tile()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Cascade()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> ArrangeAll()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Hide()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Show()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Contents()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> Index()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> OnlineHelp()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected async virtual Task<bool> LicenceInformation()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                await DoSomething();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        #endregion menu feature implementation

        #endregion Menus

        #region Controls

        /// <summary>
        /// Handle DoWork event.
        /// </summary>
        /// <typeparam name="TReturn">TReturn</typeparam>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">DoWorkEventArgs</param>
        /// <param name="workDelegate">DoWork_WorkDelegate<TReturn></param>
        /// <param name="resultNullDescription">string</param>
        public virtual void BackgroundWorker_DoWork<TReturn>
        (
            BackgroundWorker worker,
            DoWorkEventArgs e,
            DoWork_WorkDelegate<TReturn> workDelegate,
            string resultNullDescription = "No result was returned."
        )
        {
            string errorMessage = default;

            try
            {
                //run process
                if (workDelegate != null)
                {
                    e.Result =
                        workDelegate
                        (
                            worker,
                            e,
                            ref errorMessage
                        );
                }

                //look for specific problem
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new Exception(errorMessage);
                }

                //warn about unexpected result
                if (e.Result == null)
                {
                    throw new Exception(resultNullDescription);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //re-throw and let RunWorkerCompleted event handle and report error.
                throw;
            }
        }

        /// <summary>
        /// Handle ProgressChanged event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="userState">object, specifically a string.</param>
        /// <param name="progressPercentage">int</param>
        public virtual void BackgroundWorker_ProgressChanged
        (
            string description,
            object userState,
            int progressPercentage
        )
        {
            string message = string.Empty;

            try
            {
                if (userState != null)
                {
                    message = userState.ToString();
                }
                UpdateProgressBar(string.Format("{0} ({1})...{2}%", description, message, progressPercentage.ToString()), progressPercentage);
                //System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Handle RunWorkerCompleted event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">RunWorkerCompletedEventArgs</param>
        /// <param name="errorDelegate">Action<Exception>. Replaces default behavior of displaying the exception message.</param>
        /// <param name="cancelledDelegate">Action. Replaces default behavior of displaying a cancellation message. Handles the display message only; differs from cancelDelegate, which handles view-level behavior not specific to this worker.</param>
        /// <param name="completedDelegate">Action. Extends default behavior of refreshing the display; execute prior to Refresh().</param>
        public virtual void BackgroundWorker_RunWorkerCompleted
        (
            string description,
            BackgroundWorker worker,
            RunWorkerCompletedEventArgs e,
            Action<Exception> errorDelegate = null,
			Action cancelledDelegate = null,
			Action completedDelegate = null
        )
        {
            try
            {
                Exception error = e.Error;
                bool isCancelled = e.Cancelled;
                object result = e.Result;

                // First, handle the case where an exception was thrown.
                if (error != null)
                {
                    if (errorDelegate != null)
                    {
                        errorDelegate(error);
                    }
                    else
                    {
                        // Show the error message
                        StopProgressBar(null, error.Message);
                    }
                }
                else if (isCancelled)
                {
                    if (cancelledDelegate != null)
                    {
                        cancelledDelegate();
                    }
                    else
                    {
                        // Handle the case where the user cancelled the operation.
                        StopProgressBar(null, "Cancelled.");

                        //if (View.cancelDelegate != null)
                        //{
                        //    View.cancelDelegate();
                        //}
                    }
                }
                else
                {
					// Operation completed successfully, so display the result.
					completedDelegate?.Invoke();

					//backgroundworker calls New/Save without UI refresh; refresh UI explicitly here.
					ModelController<TModel>.Model.Refresh();
                }

                // Do post completion operations, like enabling the controls etc.
                //TODO:View.Activate();

                // Inform the user we're done
                StopProgressBar(description, null);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                ////clear cancellation hook
                //View.cancelDelegate = null;
            }
        }

        // /// <summary>
        // ///
        // /// </summary>
        // public void BindingSource_PositionChanged<TItem>(BindingSource bindingSource, EventArgs e, Action<TItem> itemDelegate)
        // {
        //     try
        //     {
        //         TItem current = (TItem)bindingSource.Current;

        //         if (current != null)
        //         {
        //             if (itemDelegate != null)
        //             {
        //                 itemDelegate(current);
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Handle DataError event; specifically, catch combobox cell data errors.
        // /// This allows validation message to be displayed
        // ///  and still allows the user to fix and save the settings.
        // /// </summary>
        // /// <param name="grid"></param>
        // /// <param name="e"></param>
        // /// <param name="cancel"></param>
        // /// <param name="throwException"></param>
        // /// <param name="logException"></param>
        // public void Grid_DataError
        // (
        //     DataGridView grid,
        //     DataGridViewDataErrorEventArgs e,
        //     bool? cancel,
        //     bool? throwException,
        //     bool logException
        // )
        // {
        //     try
        //     {
        //         if (cancel.HasValue)
        //         {
        //             //cancel to prevent default DataError dialogs
        //             e.Cancel = true;
        //         }

        //         if (throwException.HasValue)
        //         {
        //             //there are cases where you do not want to throw exception; because it will drop application immediately with only a log
        //             e.ThrowException = throwException.Value;
        //         }

        //         if (logException)
        //         {
        //             Log.Write(e.Exception, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }
        #endregion Controls

        #region Utility

        /// <summary>
        /// Manage buttons' state while processes are running.
        /// Usage:
        ///     View.permitEnabledStateXxx = ButtonEnabled(enabledFlag, View.permitEnabledStateXxx, View.cmdXxx, View.menuFileXxx, View.buttonFileXxx);
        /// </summary>
        /// <param name="enabledFlag">bool</param>
        /// <param name="permitEnabledState">bool</param>
        /// <param name="button">object</param>
        /// <param name="menuItem">object</param>
        /// <param name="buttonItem">object</param>
        /// <returns>updated remembered state</returns>
        public bool ButtonEnabled
        (
            bool enabledFlag,
            bool permitEnabledState,
            object button,//Note:Qml/Qml.Net control types not accessible from code-behind
            object menuItem = null,
            object buttonItem = null
        )
        {
            bool returnValue = permitEnabledState; // default(bool);
            try
            {//Note:unable to access Qml UI controls directly from code-behind
                if (enabledFlag)
                {
                    //ENABLING
                    //recall state
                    if (button != null)
                    {
                        // button.IsEnabled = permitEnabledState;
                    }
                    if (menuItem != null)
                    {
                        // menuItem.IsEnabled = permitEnabledState;
                    }
                    if (buttonItem != null)
                    {
                        // buttonItem.IsEnabled = permitEnabledState;
                    }
                }
                else
                {
                    //DISABLING
                    //remember state
                    if (button != null)
                    {
                        // returnValue = button.IsEnabled;
                    }
                    else if (menuItem != null)
                    {
                        // returnValue = menuItem.IsEnabled;
                    }
                    else if (buttonItem != null)
                    {
                        // returnValue = buttonItem.IsEnabled;
                    }

                    //disable
                    if (button != null)
                    {
                        // button.IsEnabled = enabledFlag;
                    }
                    if (menuItem != null)
                    {
                        // menuItem.IsEnabled = enabledFlag;
                    }
                    if (buttonItem != null)
                    {
                        // buttonItem.IsEnabled = enabledFlag;
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        public TIcon GetActionIconInfo(string iconName)
        {
            TIcon returnValue = default;

            try
            {
                if (string.IsNullOrEmpty(iconName))
                {
                    throw new ApplicationException("GetActionIconInfo:IsNullOrEmpty iconName=" + iconName);
                }
                returnValue = _actionIconImages[iconName];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }
        private async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //TODO:DoEvents...
                await Task.Delay(1000);
            }
        }
        #endregion Utility
        #endregion Methods

    }
}
