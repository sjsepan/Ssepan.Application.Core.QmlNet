﻿using System;
using System.ComponentModel;
using System.IO;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made re-distributable by Microsoft
using System.Reflection;
using System.Threading.Tasks;

namespace Ssepan.Application.Core.QmlNet
{
	public class DialogsController
    {
        //TODO:try a DialogsController class alongside Dialogs in this file,
        // for the purpose of handing back a singleton of Dialogs to QML,
        // since Qml apparently will not cooperate with anything static.
        public Dialogs GetNetObject()
        {
            return Dialogs.Instance;//new Dialogs();
        }

        /// <summary>
        /// Note: Several static methods are re-implemented as async methods
        /// due to the fact that MessageBox.QmlNet's Show[...] methods force async/await.
        /// </summary>
        public class Dialogs //:
            // Using Qml Signals for form, but receives PropertyChanged from Model;
            // this code-behind may need to intercept the latter and trigger the former.
            //INotifyPropertyChanged 
        {
            #region Declarations
            //Note:override the constants used by FileDialogInfo; QMl uses a different format:
            // ex: "Image files (*.jpg *.png)", "All files (*)"
            // so, "{0} (*.{1})"
            public const string FILTER_SEPARATOR = ",";
            public const string FILTER_FORMAT = "{0} (*.{1})";

            // public static Dialogs Instance = null;
            #endregion Declarations

            #region Constructors
            static Dialogs()
            {
                //TODO:DialogInfo properties and possibly calls using them, 
                // would probably behave better as instance variables 
                // (accessed from a static field like model, settings).
                Instance = new Dialogs();
            }
            #endregion Constructors

            #region INotifyPropertyChanged
            /*static*/ event PropertyChangedEventHandler PropertyChanged; //Note:RCS1159 breaks delegate add/remove below
            protected /*static*/ void OnPropertyChanged(string propertyName)
            {
				// Console.WriteLine("Dialogs:OnPropertyChanged:propertyName=" + propertyName);
				PropertyChanged?.Invoke(this/*null*/, new PropertyChangedEventArgs(propertyName));
			}
            #endregion INotifyPropertyChanged

            #region Properties
            //use something like this if we need to have an static instance
            private static Dialogs _Instance;
            public static Dialogs Instance
            {
                get { return _Instance; }
                set
                {
					if (Instance?.DefaultHandler != null)
					{
						Instance.PropertyChanged -= Instance.DefaultHandler;
					}

					_Instance = value;

					if (Instance?.DefaultHandler != null)
					{
						Instance.PropertyChanged += Instance.DefaultHandler;
					}
				}
            }

            private /*static*/ PropertyChangedEventHandler _DefaultHandler;
            /// <summary>
            /// Handler to assigned to Settings on New, Open.
            /// </summary>
            public /*static*/ PropertyChangedEventHandler DefaultHandler
            {
                get { return _DefaultHandler; }
                set
                {
                    if (DefaultHandler != null)
                    {
                        // if (Model != null)
                        // {
                            /*Model.*/PropertyChanged -= DefaultHandler;
                        // }
                    }

                    _DefaultHandler = value;

                    if (DefaultHandler != null)
                    {
                        // if (Model != null)
                        // {
                            /*Model.*/PropertyChanged += DefaultHandler;
                        // }
                    }
                }
            }
            #endregion Properties

            #region Qml.Net
            #region Events
            #region DialogInfo
            private /*static*/ void OnShowMessageDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowMessageDialog:" + dialogInfoType);
                    //qml file will be watching for onShowMessageDialog
                    // Qml.Net.Signals.ActivateSignal("showMessageDialog", dialogInfoType);
                    OnPropertyChanged("Message");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            private /*static*/ void OnShowAboutDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowAboutDialog:" + dialogInfoType);
                    //qml file will be watching for onShowAboutDialog
                    // Qml.Net.Signals.ActivateSignal("showAboutDialog", dialogInfoType);
                    OnPropertyChanged("About");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            private /*static*/ void OnShowFontDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowFontDialog:" + dialogInfoType);
                    //qml file will be watching for onShowFontDialog
                    // Qml.Net.Signals.ActivateSignal("showFontDialog", dialogInfoType);
                    OnPropertyChanged("Font");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            private /*static*/ void OnShowColorDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowColorDialog:" + dialogInfoType);
                    //qml file will be watching for onShowColorDialog
                    // Qml.Net.Signals.ActivateSignal("showColorDialog", dialogInfoType);
                    OnPropertyChanged("Color");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            private /*static*/ void OnShowFileDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowFileDialog:" + dialogInfoType);
                    //qml file will be watching for onShowFileDialog
                    // Qml.Net.Signals.ActivateSignal("showFileDialog", dialogInfoType);
                    OnPropertyChanged("File");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            protected /*static*/ void OnShowPrinterDialog(string dialogInfoType)
            {
                try
                {
                    // Console.WriteLine("OnShowPrinterDialog:" + dialogInfoType);
                    //qml file will be watching for onShowFileDialog
                    // Qml.Net.Signals.ActivateSignal("showPrinterDialog", dialogInfoType);
                    OnPropertyChanged("Printer");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
			#endregion DialogInfo
			#endregion Events

			#region Properties
			#region DialogInfo
			//<object, ButtonRole, object, string, StandardButtons, string>
			public /*static*/ MessageDialogInfo<object, string, object, string, string> MsgDialogInfo { get; set; }
			public /*static*/ AboutDialogInfo<object, string, string> AbtDialogInfo { get; set; }
			public /*static*/ FontDialogInfo<object, string, string> FntDialogInfo { get; set; }
			public /*static*/ ColorDialogInfo<object, string, string> ClrDialogInfo { get; set; }
			public /*static*/ FileDialogInfo<object, string> FilDialogInfo { get; set; }
			public /*static*/ PrinterDialogInfo<object, string, string> PrtDialogInfo { get; set; }
			#endregion DialogInfo
			#endregion Properties
			#endregion Qml.Net

			#region Methods
			/// <summary>
			/// Get path to save data.
			/// Triggers QmlNet signal OnShowFileDialog.
			/// </summary>
			/// <param name="fileDialogInfo">FileDialogInfo<object, string></param>
			/// <returns>Task<FileDialogInfo<object, string>></returns>
			public async /*static*/ Task<FileDialogInfo<object, string>> GetPathForSave
            (
                FileDialogInfo<object, string> fileDialogInfo
            )
            {
				try
				{
                    if
                    (
                        fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
                        ||
                        fileDialogInfo.ForceDialog
                    )
                    {
                        FilDialogInfo = fileDialogInfo;
                        FilDialogInfo.Busy = true;
                        OnShowFileDialog("File");
                        await WaitDialogFree(FilDialogInfo);
                        //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                        if (!string.IsNullOrWhiteSpace(fileDialogInfo.Filename))
                        {
                            if (string.Equals(Path.GetFileName(fileDialogInfo.Filename), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
                            {
								//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
								string messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
								// string[] nameAndPattern = null;
								MessageDialogInfo<object, string, object, string, string> messageDialogInfo =
                                    new (
                                        parent: fileDialogInfo.Parent,
                                        modal: fileDialogInfo.Modal,
                                        title: fileDialogInfo.Title,
                                        dialogFlags: null,
                                        messageType: "Info",
                                        buttonsType: "Ok",
                                        message: messageTemp,
                                        response: null
                                    );

								// Dialogs d = new Dialogs();
								// ShowMessageDialog
								// (
								//     ref messageDialogInfo,
								//     ref errorMessage
								// ); 
								messageDialogInfo = await /*Dialogs.*/ShowMessageDialog(messageDialogInfo);
                                // d = null;

                                //Forced cancel
                                fileDialogInfo.Response = "Cancel";
                            }
                            else
                            {
                                //let FileDialogInfo return in fileDialoginfo
                                fileDialogInfo = FilDialogInfo;
                            }
                        }
                    }
                    else
                    {
                        //set directly
                        fileDialogInfo.BoolResult = true;
                    }
                }
                catch (Exception ex)
                {
                    fileDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                    throw;
                }

                return fileDialogInfo;
            }

            /// <summary>
            /// Get path to load data.
            /// Triggers QmlNet signal OnShowFileDialog.
            /// </summary>
            /// <param name="fileDialogInfo">FileDialogInfo<object, string></param>
            /// <returns>Task<FileDialogInfo<object, string>></returns>
            public async /*static*/ Task<FileDialogInfo<object, string>> GetPathForLoad
            (
                FileDialogInfo<object, string> fileDialogInfo
            )
            {
                try
                {
                    if (fileDialogInfo.ForceNew)
                    {
                        fileDialogInfo.Filename = fileDialogInfo.NewFilename;
                        fileDialogInfo.BoolResult = true;
                    }
                    else
                    {
                        FilDialogInfo = fileDialogInfo;
                        FilDialogInfo.Busy = true;
                        OnShowFileDialog("File");
                        await WaitDialogFree(FilDialogInfo);
                        //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                        //ensure FilDialogInfo return in fileDialoginfo
                        fileDialogInfo = FilDialogInfo;
                    }
                }
                catch (Exception ex)
                {
                    fileDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                    throw;
                }

                return fileDialogInfo;
            }

            /// <summary>
            /// Select a folder path.
            /// Triggers QmlNet signal OnShowFileDialog.
            /// </summary>
            /// <param name="fileDialogInfo">returns path in Filename property if fileDialogInfo</param>
            /// <returns>fileDialogInfo</returns>
            public async /*static*/ Task<FileDialogInfo<object, string>> GetFolderPath
            (
                FileDialogInfo<object, string> fileDialogInfo
            )
            {
                try
                {
                    FilDialogInfo = fileDialogInfo;
                    FilDialogInfo.Busy = true;
                    OnShowFileDialog("File");
                    await WaitDialogFree(FilDialogInfo);
                    //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                    fileDialogInfo = FilDialogInfo;
                }
                catch (Exception ex)
                {
                    fileDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }

                return fileDialogInfo;
            }

            /// <summary>
            /// Shows a message dialog.
            /// Triggers QmlNet signal OnShowMessageDialog.
            /// </summary>
            /// <param name="messageDialogInfo">MessageDialogInfo<object, string, object, string, string></param>
            /// <returns>Task<MessageDialogInfo<object, string, object, string, string>></returns>
            public async /*static*/ Task<MessageDialogInfo<object, string, object, string, string>> ShowMessageDialog
            (
                MessageDialogInfo<object, string, object, string, string> messageDialogInfo
            )
            {
                try
                {
                    // Use custom signal to invoke dialog in Qml, 
                    // passing dialogInfo and awaiting for semaphore inside same when finished.
                    MsgDialogInfo = messageDialogInfo;

                    MsgDialogInfo.Busy = true;
                    OnShowMessageDialog("Message");
                    await WaitDialogFree(MsgDialogInfo);
                    //Console.WriteLine("MsgDialogInfo.Response:" + MsgDialogInfo.Response);

                    //ensure MsgDialogInfo return in messageDialogInfo
                    messageDialogInfo = MsgDialogInfo;
                }
                catch (Exception ex)
                {
                    messageDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }

                return messageDialogInfo;
            }

            /// <summary>
            /// Shows an About dialog.
            /// Triggers QmlNet signal OnShowAboutDialog.
            /// </summary>
            /// <param name="aboutDialogInfo">AboutDialogInfo<object, string, string></param>
            /// <returns>Task<AboutDialogInfo<object, string, string>></returns>
            public async /*static*/ Task<AboutDialogInfo<object, string, string>> ShowAboutDialog
            (
                AboutDialogInfo<object, string, string> aboutDialogInfo
            )
            {
                try
                {
                    // Use custom signal to invoke dialog in Qml, 
                    // passing dialogInfo and awaiting for semaphore inside same when finished.
                    AbtDialogInfo = aboutDialogInfo;

                    AbtDialogInfo.Busy = true;
                    OnShowAboutDialog("About");
                    await WaitDialogFree(AbtDialogInfo);
                    //Console.WriteLine("AbtDialogInfo.Response:" + AbtDialogInfo.Response);

                    //ensure AbtDialogInfo return in aboutDialogInfo
                    aboutDialogInfo = AbtDialogInfo;
                }
                catch (Exception ex)
                {
                    aboutDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }

                return aboutDialogInfo;
            }

            /// <summary>
            /// Get a printer.
            /// Triggers QmlNet signal OnShowPrinterDialog.
            /// </summary>
            /// <param name="printerDialogInfo">PrinterDialogInfo<object, string, string></param>
            /// <returns>Task<PrinterDialogInfo<object, string, string>></returns>
            public async /*static*/ Task<PrinterDialogInfo<object, string, string>> GetPrinter
            (
                PrinterDialogInfo<object, string, string> printerDialogInfo
            )
            {
                try
                {
                    // Use custom signal to invoke dialog in Qml, 
                    // passing dialogInfo and awaiting for semaphore inside same when finished.
                    PrtDialogInfo = printerDialogInfo;

                    PrtDialogInfo.Busy = true;//Note:no print dialog accessible in QML.Net; nobody will be listening yet (except dummy messagebox)
                    OnShowPrinterDialog("Printer");
                    await WaitDialogFree(PrtDialogInfo);

                    //ensure PrtDialogInfo return in printerDialogInfo
                    printerDialogInfo = PrtDialogInfo;
                }
                catch (Exception ex)
                {
                    printerDialogInfo.ErrorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }

                return printerDialogInfo;
            }

            /// <summary>
            /// Get a color.
            /// Triggers QmlNet signal OnShowColorDialog.
            /// </summary>
            /// <param name="colorDialogInfo">ColorDialogInfo<object, string, string></param>
            /// <returns>Task<ColorDialogInfo<object, string, string>></returns>
            public async /*static*/ Task<ColorDialogInfo<object, string, string>> GetColor
            (
                ColorDialogInfo<object, string, string> colorDialogInfo
            )
            {
                try
                {
                    // Use custom signal to invoke dialog in Qml, 
                    // passing dialogInfo and awaiting for semaphore inside same when finished.
                    ClrDialogInfo = colorDialogInfo;

                    ClrDialogInfo.Busy = true;
                    OnShowColorDialog("Color");
                    await WaitDialogFree(ClrDialogInfo);

                    //ensure ClrDialogInfo return in colorDialogInfo
                    colorDialogInfo = ClrDialogInfo;
                }
                catch (Exception ex)
                {
                    colorDialogInfo.ErrorMessage = ex.Message;
                    Console.WriteLine(ex.Message);
                }

                return colorDialogInfo;
            }

            /// <summary>
            /// Get a font descriptor.
            /// Triggers QmlNet signal OnShowFontDialog.
            /// </summary>
            /// <param name="fontDialogInfo">FontDialogInfo<object, string, string></param>
            /// <returns>Task<FontDialogInfo<object, string, string>></returns>
            public async /*static*/ Task<FontDialogInfo<object, string, string>> GetFont
            (
                FontDialogInfo<object, string, string> fontDialogInfo
            )
            {
                try
                {
                    // Use custom signal to invoke dialog in Qml, 
                    // passing dialogInfo and awaiting for semaphore inside same when finished.
                    FntDialogInfo = fontDialogInfo;

                    FntDialogInfo.Busy = true;
                    OnShowFontDialog("Font");
                    await WaitDialogFree(FntDialogInfo);
                    //Console.WriteLine("FntDialogInfo.Response:" + FntDialogInfo.Response);

                    //ensure FntDialogInfo return in fontDialogInfo
                    fontDialogInfo = FntDialogInfo;
                }
                catch (Exception ex)
                {
                    fontDialogInfo.ErrorMessage = ex.Message;
                    Console.WriteLine(ex.Message);
                }

                return fontDialogInfo;
            }

            /// <summary>
            /// Perform input of connection string and provider name.
            /// Uses MS Data Connections Dialog.
            /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
            /// </summary>
            /// <param name="connectionString">ref string</param>
            /// <param name="providerName">ref string</param>
            /// <param name="errorMessage">ref string</param>
            /// <returns>bool</returns>
            public /*static*/ bool GetDataConnection
            (
                ref string connectionString,
                ref string providerName,
                ref string errorMessage
            )
            {
                bool returnValue = default;
                //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
                //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

                try
                {
                    //dataConnectionDialog = new DataConnectionDialog();

                    //DataSource.AddStandardDataSources(dataConnectionDialog);

                    //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                    //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

                    //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                    //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                    //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data CConnection Dialog.
                    //dataConnectionDialog.ConnectionString = connectionString;

                    if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                    {
                        ////extract connection string
                        //connectionString = dataConnectionDialog.ConnectionString;
                        //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                        ////writes provider selection to xml file
                        //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                        ////save these too
                        //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                        //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                        returnValue = true;
                    }
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }
                return returnValue;
            }

            #region utility

            /// <summary>
            /// Methods can return .NET types.
            /// The returned type can be invoked from Qml (properties/methods/events/etc),
            ///  even if it has not been registered.
            /// Not used in this implementation.
            /// </summary>
            /// <returns>Dialogs</returns>
            public static Dialogs CreateNetObject()
            {
                return Instance;
            }

            private /*static*/ async Task WaitDialogFree(DialogInfoBase<object, string> dialogInfo)
            {
                // Console.WriteLine("WaitDialogFree,start,Busy:" + dialogInfo.Busy.ToString());
                while (dialogInfo.Busy)
                {
                    await Task.Delay(1000);
                    // Console.WriteLine("WaitDialogFree,loop,Busy:" + dialogInfo.Busy.ToString());
                }
                // Console.WriteLine("WaitDialogFree,end,Busy:" + dialogInfo.Busy.ToString());
            }
            #endregion utility
            #endregion Methods
        }
    }
}
